#!/usr/bin/env python

'''
Arlo Hoogeveen
9/1/2014
wanted to make something for downloading pdb files
'''

from ftplib import FTP

pdbUrl = "ftp.wwpdb.org"
pdbFolder = "/pub/pdb/data/structures/divided/pdb/"
destenation = "./pdb files/"


def pdbNameGrab(localFolder):
    output = []
    ftp.cwd(localFolder)
    ftp.retrlines('NLST', output.append)
    return output

def download(localFolder,filename):
    out = open(destenation+filename, 'w')
    ftp.cwd(localFolder)
    ftp.retrbinary('RETR '+filename,out.write)

if __name__ == '__main__':
    #logging on to server
    ftp = FTP(pdbUrl)
    ftp.login()
    #what do i have
    skipfolders = open("skipthis.txt",'r').read().split('\n')
    #quiry for sub folder names
    folders = pdbNameGrab(pdbFolder)
    for i in folders:
        if i in skipfolders:
            print "skipping "+i
        else:
            #now we see what files are in there
            newFolder = pdbFolder+i
            print i
            for j in pdbNameGrab(newFolder):
                #and download each filename
                print "downloading: "+j
                download(newFolder,j)