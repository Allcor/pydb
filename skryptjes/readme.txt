there are hardly any notes, so let me at least write down the workflow.



---folders you need to create, won't do it for you.---
./files/   < for pdb files
./data/   < for the extracted tempeture data (i know ambiguous names)
will create lot of files in current folder



so the pdbFileDownload is redundant, you can do the same from there ftp website of pdb.

1. pydb_dataExtract
when you have the pdb's (gzipped) i used pydb_dataExtract to extract the pdb_code, acusition method and temperature. And some additional info, of witch it turned out i didn't need.

2. pydb_tempgraph
to group the data I created pydb_tempgraph, initialy just to make a distribution graph, hence the name. It now creates 5 bins of the NMR data. 

3. external filter
the resulting files (when below 5000 pdb_id's) can be copied in pdb filter select.
http://bioinfo.mni.th-mh.de/pdbselect/pdbfilter-select/
this will make a representative selection of your data, eliminating warped resaults coused by redundant structures.

so coppy the whole content of the files put out by the previous code into the 'input chains' funktion of the site. The output select file with the 1-5, 2-5, exc behind it. can be changed in first lines of the foloing codes (imput).

4. makingDssp
For the water acceseble area we will need DSSP files. I created this scrypt to do most of it for me. Some of them won't work and need to be done by hand so read the command line for the errors givven by dssp.

5. pydb_matrix
if you want to do the rest of the calculations with R this script will make a matrix that can confieniently be loaded into R. For more info on these scrypts you better ask Erik.

to get the pp and prop readings from python scrypt you can use pydb_contacts_wateronly.py and pydb_contacts.py. These are far from done and i advise to run pydb_matrix.py first to create a list of pdb files to skip.
further reasurtch could move this before the external filter-select step. Potentialy selecting more structures. This whould mean that dssp needs to be done on the whole pdb set. Finding out why some dssp files only are made manual whould help here.
