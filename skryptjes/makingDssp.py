#! /usr/bin/env python
'''
20-1-2014
Arlo Hoogeveen
creating dssp files from the pdb's i selected.
'''

import gzip, os

mainfolder = '../'
basedoc = 'selectfile|-5.txt'
dsspFolder = 'DSSP_files/'
basePDB = 'pdb_files/pdb|.ent.gz'

for i in range(1,6):
    imput = mainfolder+basedoc.split('|')[0]+str(i)+basedoc.split('|')[1]
#if __name__ == '__main__':
    #imput = 'pdb select.txt'
    #print 'opening '+imput
    pdblist = open(imput,'r')
    for line in pdblist.readlines():
        if line[0] == '#':pass
        else:
            pdbId = line[8:12]
            pdbName = basePDB.split('|')[0]+str(pdbId).lower()+basePDB.split('|')[1]
            dsspName = mainfolder+dsspFolder+pdbId.upper()+'.dssp'
            if os.path.isfile(dsspName):
                None
            else:
                pdbDoc = open(pdbId+'.pdb', 'w')
                data = gzip.open(mainfolder+pdbName, 'r')
                for line in data.readlines():
                    pdbDoc.write(line)
                print 'creating dssp for '+pdbId
                os.system('/usr/bin/dssp -i '+pdbId+'.pdb -o '+dsspName)
                os.remove(pdbId+'.pdb')
print "done"