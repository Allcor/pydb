#!/usr/bin/env python

'''
Arlo Hoogeveen
13/1/2014
so extracting the actual pdb and safing file name and tempeture
'''

import gzip,glob

folder = "../pdb_files/"
safeFileraw = "tempeturedata.txt"
safeFolder = "./tempdata/"
errorlog = open("errorlog.txt",'w')

def openGzip(filename):
    data = gzip.open(filename, 'r')
    return data

def tempCheck(temp,pdbId):
    splittemp = temp.split(';')
    if len(splittemp)>0:
        if len(splittemp)>1:
            errorlog.write(pdbId+'\t')
            errorlog.write(temp+'\n')
        return ','.join(splittemp)
    else:
        return temp

def SafeFile(count,safefile,safefolder):
    dus = safefolder+safefile.split('.')[0]+str(count)+"."+safefile.split('.')[1]
    return dus


if __name__ == '__main__':
    print "starting"
    fileList = glob.glob(folder+"*.ent.gz")
    pdblib = {}
    counter = 0
    count = 0
    count2 = 0
    safe = SafeFile(count,safeFileraw,safeFolder)
    output = open(safe,'w')
    
    for i in fileList:
        pdbId = i[16:].split('.')[0]
        data = openGzip(i)
        temp = 'NULL'
        info = ""
        method = 'NULL'
        try:
            for line in data.readlines():
                if "REMARK" in line:
                    if "TEMPERATURE           (KELVIN)" in line:
                        lineEdit = line.strip().split(':')
                        temp = tempCheck(lineEdit[1],pdbId)
                    elif "TEMPERATURE (KELVIN)" in line: #retard check ...
                        lineEdit = line.strip().split(':')
                        temp = tempCheck(lineEdit[1],pdbId)
                    #elif "TEMPERATURE" in line:
                        #errorlog.write(line)
                        #print "wtf is this: \n"+line
                    elif "EXPERIMENT TYPE" in line:
                        lineEdit = line.strip().split(':')
                        method = lineEdit[1]
                elif "COMPND" in line:
                    if info == "":
                        info = line.strip()[10:]
                    else:
                        info += line.strip()[11:]
        except:
            print i+" might be corrupt"
        output.write('ID: '+pdbId+';TEMP:'+str(temp)+';METHOD: '+method+';'+info+'\n')
        counter += 1
        if counter == 1000: #for each 1000 pdb's read i write the data into a file.
            output.close()
            count += 1
            print str(count*1000)+" of 96,809"
            safe = SafeFile(count,safeFileraw,safeFolder)
            print 'creating '+safe
            output = open(safe,'w')
            counter = 0
        elif counter%100 == 0:
            count2 += 1
            print str(count2*100)+" of 96,809"
    errorlog.close()
    output.close()
    print "done"