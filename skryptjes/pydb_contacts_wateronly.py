#! /usr/bin/env python
'''
19-2-2014
Arlo Hoogeveen
calculating water potentials + propensities
'''

import math,gzip

k = 1#1.3806488 #10^23
T = 1#298 # 25 degrees celcius
#res = {0:'ALA',1:'ARG',2:'ASN',3:'ASP',4:'CYS',5:'GLU',6:'GLN',7:'GLY',8:'HIS',9:'ILE',10:'LEU',11:'LYS',12:'MET',13:'PHE',14:'PRO',15:'SER',16:'THR',17:'TRP',18:'TYR',19:'VAL'}
#resNr = {'ALA':0,'ARG':1,'ASN':2,'ASP':3,'CYS':4,'GLU':5,'GLN':6,'GLY':7,'HIS':8,'ILE':9,'LEU':10,'LYS':11,'MET':12,'PHE':13,'PRO':14,'SER':15,'THR':16,'TRP':17,'TYR':18,'VAL':19}
res = {0:'CYS',1:'PHE',2:'LEU',3:'TRP',4:'VAL',5:'ILE',6:'MET',7:'HIS',8:'TYR',9:'ALA',10:'GLY',11:'PRO',12:'ASN',13:'THR',14:'SER',15:'ARG',16:'GLN',17:'ASP',18:'LYS',19:'GLU',20:'HOH'}
resNr = {'CYS':0,'PHE':1,'LEU':2,'TRP':3,'VAL':4,'ILE':5,'MET':6,'HIS':7,'TYR':8,'ALA':9,'GLY':10,'PRO':11,'ASN':12,'THR':13,'SER':14,'ARG':15,'GLN':16,'ASP':17,'LYS':18,'GLU':19,'HOH':20}
#resSurfArea = {'CYS':127,'PHE':201,'LEU':203,'TRP':273,'VAL':201,'ILE':217,'MET':252,'HIS':257,'TYR':288,'ALA':174,'GLY':143,'PRO':174,'ASN':200,'THR':158,'SER':202,'ARG':276,'GLN':246,'ASP':229,'LYS':256,'GLU':254}
resSurfArea = {'ARG':241,'TRP':259,'TYR':229,'LYS':211,'PHE':218,'MET':204,'GLN':189,'HIS':194,'GLU':183,'LEU':180,'ILE':182,'ASN':158,'ASP':151,'CYS':140,'VAL':160,'THR':146,'PRO':143,'SER':122,'ALA':113,'GLY':85}
resLetter = {'C':'CYS','F':'PHE','L':'LEU','W':'TRP','V':'VAL','I':'ILE','M':'MET','H':'HIS','Y':'TYR','A':'ALA','G':'GLY','P':'PRO','N':'ASN','T':'THR','S':'SER','R':'ARG','Q':'GLN','D':'ASP','K':'LYS','E':'GLU'}

output = "matrix.txt"
imput = ["../selectfile1-5.txt","../selectfile2-5.txt","../selectfile3-5.txt","../selectfile4-5.txt","../selectfile5-5.txt"]
pdbfolder = '../pdb_files/pdb'
dsspFolder = "../DSSP_files/"
filter_file = "./errorlog.txt"
filter = []
for line in open(filter_file,'r').readlines():
    for id in line.strip().split('\t'):
        filter.append(id)

def pairpot(c,n,x,y):
    ksum = 0
    for i in n.keys():
        ksum += n[i]
    w = float((n[res[x]]*n[res[y]]))/float(ksum)
    pp = -1*k*T*math.log(c[x][y]/w)
    return pp

def prop(buried,not_buried,a):
    total_not = 0
    total_bur = 0
    total_all = 0
    for i in range(len(buried)):
        total_not += not_buried[i]
        total_bur += buried[i]
        total_all += buried[i]+not_buried[i]
    #official method
    prop = (float(not_buried[a])/float(total_not))/(float((buried[a]+not_buried[a])/float(total_all)))
    #adjusted method
    #prop[a] = (float(buried[a])/float(not_buried[a]))/(float(total_buried)/float(total_not))
    return prop


def readAllChains(imput_local):
    sequenceDist=2
    sqCutoff = 12.0**2.0
    buried_cutoff = 0.07
    n = {}
    #q = {} # could remove q, was just to check out if 4 comes close
    for r in resNr.keys():
        n[r] = 0
    c = [[0]*21 for i in range(21)]
    buried = [0]*21
    not_buried = [0]*21
    pdblist = open(imput_local,'r')
    for line in pdblist.readlines():
        if line[0] == '#':pass
        else:
            try:
                pdbId = line[8:12]
                chain_id = line[12]
                print 'reading: '+pdbId +' '+chain_id
                if pdbId in filter:
                    pass
                else:
                    residues = {} #clearing privious chain't residue info
                    filename = pdbfolder+pdbId.lower()+'.ent.gz'
                    try:
                        infile = gzip.open(filename, 'r') 
                    except IOError: # In case of IOError return empty collection
                        print "Error: Cannot open PDB file " + filename + "."
                        # exit(1)
                        return
                    #water contacts
                    dsspdoc = open(dsspFolder+pdbId+'.dssp','r')
                    for line in dsspdoc.readlines():
                        if line.strip()[-1] == '.':pass
                        elif line.strip()[0] == '#':pass
                        else:
                            if line[11] == chain_id:
                                res_num = int(line[5:10].strip())
                                if line[13] == 'X':
                                    print "skipping res "+str(res_num)
                                else:
                                    #aa_type can be a lowercase letter in the dssp, indicating CYS sulphur bindings, a with a, b with b exc. 
                                    if line[13].islower():
                                        #print "lower :"+ line[13]+' in '+pdbId
                                        aa_type = 'CYS'
                                    else:
                                        aa_type = resLetter[line[13]]
                                    acc = float(line[34:38].strip())
                                    #if acc > resSurfArea[aa_type]:
                                    #    print aa_type+" max surface should be "+str(acc)
                                    ar = acc/resSurfArea[aa_type]
                                    solv_contact = int(ar*4+0.5)
                                    n[aa_type] += 4
                                    n['HOH'] += solv_contact
                                    c[resNr[aa_type]][resNr['HOH']] += solv_contact
                                    #now for the propensities
                                    if ar < buried_cutoff:
                                        buried[resNr[aa_type]] += 1
                                    else:
                                        not_buried[resNr[aa_type]] += 1
                    dsspdoc.close()
            except KeyError:
                print "skipping "+pdbId+", KeyError (probebly pdb and dssp don't line up)"
    pdblist.close()
    return[c,n,buried,not_buried]



if __name__ == "__main__":
    #making n,c and som
    foo = {}
    c = []
    n = []
    bur = []
    un_bur = []
    for i in range(len(imput)):
        foo[i] = None
        imput2=imput[i]
        foo[i] = readAllChains(imput2)
        c.append(foo[i][0])
        n.append(foo[i][1])
        bur.append(foo[i][2])
        un_bur.append(foo[i][3])
    output = open(output,'w')
    #output.write("temp range: 265-296\n")
    output.write("aa\tprop_1\tpote_1\tbur_1\tun_bur_1\tprop_2\tpote_2\tbur_2\tun_bur_2\tprop_3\tpote_3\tbur_3\tun_bur_3\tprop_4\tpote_4\tbur_4\tun_bur_4\tprop_5\tpote_5\tbur_5\tun_bur_5\n")
    for a in range(20):
        output.write(res[a])
        for temp in range(len(imput)):
            output.write('\t'+str(math.log(prop(bur[temp],un_bur[temp],a)))+'\t'+str(pairpot(c[temp],n[temp],a,20))+'\t'+str(bur[temp][a])+'\t'+str(un_bur[temp][a]))
        output.write('\n')
    output.close()
    print "done"