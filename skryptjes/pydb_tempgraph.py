#!/usr/bin/env python

'''
Arlo Hoogeveen
16/1/2014
trying to make a tempeture distribution of the pdb's
'''

import glob


tempDicX = {}
tempDicR = {}
tempDicF = {}
tempDicN = {}
folder = '../relevant pdb data/'
output = "tempgraphinfo.txt"
skiplist = []

if __name__ == '__main__':
    for t in range(0,451):
        tempDicX[t] = []
        tempDicR[t] = []
        tempDicF[t] = []
        tempDicN[t] = []
    fileList = glob.glob(folder+"*.txt")
    for i in fileList:
        pdbfile = open(i,'r')
        for line in pdbfile.readlines():
            data = line.split(';')
            ID = data[0].split(':')[1].strip()
            temp = data[1].split(':')[1].split(',')
            for t in temp:
                if "NULL" in t or "-" in t or t == "":
                    skiplist.append(data[0])
                else:
                    #print t
                    t=int(float(t.replace("K", "")))
                    if "NMR" in data[2]:
                        tempDicR[t].append(ID.upper())
                    elif "X-RAY DIFFRACTION" in data[2]:
                        tempDicX[t].append(ID.upper())
                    elif "FIBER DIFFRACTION" in data[2] or "THEORETICAL MODELLING" in data[2] or "SMALL ANGLE X-RAY SCATTERING" in data[2] or "NEUTRON DIFFRACTION" in data[2] or "SMALL ANGLE NEUTRON SCATTERING" in data[2] or "X-RAY POWDER DIFFRACTION" in data[2] or "ELECTRON DIFFRACTION" in data[2]:
                        tempDicF[t].append(ID.upper())
                    else:
                        tempDicN[t].append(ID.upper())
        print "end file"
    doc = open("skiplist.txt","w")
    doc.write('; '.join(skiplist))
    doc.close()
    doc = open("NMR1-5.txt",'w')
    for t in range(265,290):
        doc.write('\t'.join(tempDicR[t]))
        if len(tempDicR[t])>0:doc.write('\t')
    doc.close()
    doc = open("NMR2-5.txt",'w')
    for t in range(291,296):
        doc.write('\t'.join(tempDicR[t]))
        if len(tempDicR[t])>0:doc.write('\t')
    doc.close()
    doc = open("NMR3-5.txt",'w')
    for t in range(297,299):
        doc.write('\t'.join(tempDicR[t]))
        if len(tempDicR[t])>0:doc.write('\t')
    doc.close()
    doc = open("NMR4-5.txt",'w')
    for t in range(300,305):
        doc.write('\t'.join(tempDicR[t]))
        if len(tempDicR[t])>0:doc.write('\t')
    doc.close()
    doc = open("NMR5-5.txt",'w')
    for t in range(306,340):
        doc.write('\t'.join(tempDicR[t]))
        if len(tempDicR[t])>0:doc.write('\t')
    doc.close()
    output = open(output,'w')
    output.write("temperature;X-RAY DIFFRACTION;MNR;other\n")
    for t in range(0,451):
        output.write(str(t)+';'+str(tempDicX[t])+";"+str(tempDicR[t])+';'+str(tempDicF[t])+'\n')
    output.close()
    print "done"