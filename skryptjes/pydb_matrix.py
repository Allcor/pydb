#! /usr/bin/env python
'''
13-3-2014
Arlo Hoogeveen
creating a matrix for mixing data
'''

import math,gzip,re

k = 1#1.3806488 #10^23
T = 1#298 # 25 degrees celcius
#res = {0:'ALA',1:'ARG',2:'ASN',3:'ASP',4:'CYS',5:'GLU',6:'GLN',7:'GLY',8:'HIS',9:'ILE',10:'LEU',11:'LYS',12:'MET',13:'PHE',14:'PRO',15:'SER',16:'THR',17:'TRP',18:'TYR',19:'VAL'}
#resNr = {'ALA':0,'ARG':1,'ASN':2,'ASP':3,'CYS':4,'GLU':5,'GLN':6,'GLY':7,'HIS':8,'ILE':9,'LEU':10,'LYS':11,'MET':12,'PHE':13,'PRO':14,'SER':15,'THR':16,'TRP':17,'TYR':18,'VAL':19}
res = {0:'CYS',1:'PHE',2:'LEU',3:'TRP',4:'VAL',5:'ILE',6:'MET',7:'HIS',8:'TYR',9:'ALA',10:'GLY',11:'PRO',12:'ASN',13:'THR',14:'SER',15:'ARG',16:'GLN',17:'ASP',18:'LYS',19:'GLU',20:'HOH'}
resNr = {'CYS':0,'PHE':1,'LEU':2,'TRP':3,'VAL':4,'ILE':5,'MET':6,'HIS':7,'TYR':8,'ALA':9,'GLY':10,'PRO':11,'ASN':12,'THR':13,'SER':14,'ARG':15,'GLN':16,'ASP':17,'LYS':18,'GLU':19,'HOH':20}
#resSurfArea = {'CYS':127,'PHE':201,'LEU':203,'TRP':273,'VAL':201,'ILE':217,'MET':252,'HIS':257,'TYR':288,'ALA':174,'GLY':143,'PRO':174,'ASN':200,'THR':158,'SER':202,'ARG':276,'GLN':246,'ASP':229,'LYS':256,'GLU':254}
resSurfArea = {'ARG':241,'TRP':259,'TYR':229,'LYS':211,'PHE':218,'MET':204,'GLN':189,'HIS':194,'GLU':183,'LEU':180,'ILE':182,'ASN':158,'ASP':151,'CYS':140,'VAL':160,'THR':146,'PRO':143,'SER':122,'ALA':113,'GLY':85}
resLetter = {'C':'CYS','F':'PHE','L':'LEU','W':'TRP','V':'VAL','I':'ILE','M':'MET','H':'HIS','Y':'TYR','A':'ALA','G':'GLY','P':'PRO','N':'ASN','T':'THR','S':'SER','R':'ARG','Q':'GLN','D':'ASP','K':'LYS','E':'GLU'}

output = "matrix5bins.txt"
imput = ["../selectfile1-5.txt","../selectfile2-5.txt","../selectfile3-5.txt","../selectfile4-5.txt","../selectfile5-5.txt"]
pdbfolder = '../pdb_files/pdb'
dsspFolder = "../DSSP_files/"
filter_file = "./errorlog.txt"
filter_append_list = []


def temp_extract(pdbId): 
    filename = pdbfolder+pdbId.lower()+'.ent.gz'
    try:
        pdbfile = gzip.open(filename, 'r') 
    except IOError: # In case of IOError return empty collection
        print "Error: Cannot open PDB file " + filename + "."
        return
    for line in pdbfile.readlines():
        if "REMARK" in line:
            if "TEMPERATURE           (KELVIN)" in line:
                lineEdit = line.strip().split(':')
                temp = lineEdit[1]
            elif "TEMPERATURE (KELVIN)" in line: #retard check ...
                lineEdit = line.strip().split(':')
                temp = lineEdit[1]
    if re.search('[a-zA-Z;]+',temp):
        if re.search('[;]+',temp):
            items = temp.split(';')
            if all(x == items[0] for x in items):
                #print "all the same, so using it"
                return items[0]
            else:
                filter_append_list.append(pdbId)
                print pdbId+" has multiple temperatures: "+temp
        else:
            filter_append_list.append(pdbId)
            print pdbId+" has a dubious temp: "+temp
    else:
        return temp

def readChain(pdbId,chain_id):
    sequenceDist=2
    sqCutoff = 12.0**2.0
    buried_cutoff = 0.07
    Choh = {}
    Caa = {}
    buried = {}
    not_buried = {}
    for r in resNr.keys():
        if r == 'HOH': pass
        else:
                Choh[r] = 0
                Caa[r] = 0
                buried[r] = 0
                not_buried[r] = 0
    #print 'reading: '+pdbId +' '+chain_id
    filename = dsspFolder+pdbId+'.dssp'
    try:
            dsspdoc = open(filename,'r')
    except IOError: # In case of IOError return empty collection, some dssp's could not be made
        print "Error: Cannot open DSSP file " + filename + "."
        filter_append_list.append(pdbId)
        return
    for line in dsspdoc.readlines():
        if line.strip()[-1] == '.':pass
        elif line.strip()[0] == '#':pass
        else:
            if line[11] == chain_id:
                res_num = int(line[5:10].strip())
                if line[13] == 'X':
                    print "skipping res "+str(res_num)+" in "+pdbId
                else:
                    #aa_type can be a lowercase letter in the dssp, indicating CYS sulphur bindings, a with a, b with b exc. 
                    if line[13].islower():
                        #print "lower :"+ line[13]+' in '+pdbId
                        aa_type = 'CYS'
                    else:
                        aa_type = resLetter[line[13]]
                        acc = float(line[34:38].strip())
                        ar = acc/resSurfArea[aa_type]
                        solv_contact = int(ar*4+0.5)
                        Caa[aa_type] += 4-solv_contact
                        Choh[aa_type] += solv_contact
                        #now for the propensities
                        if ar <= buried_cutoff:
                            buried[aa_type] += 1
                        else:
                            not_buried[aa_type] += 1
        dsspdoc.close()
    return (buried,not_buried,Choh,Caa)

if __name__ == "__main__":
    #creating filter list
    filter = []
    miep = open(filter_file,'r')
    for line in miep.readlines():
        for id in line.strip().split('\t'):
            filter.append(id)
    miep.close()
    #writing matrix
    output = open(output,'w')
    #labels
    output.write('PDB_chainId\tTemperature\t')
    for i in range(21):
        r = res[i]
        if r == 'HOH': pass
        else:
            output.write(r+'_bur\t'+r+'_unbur\t'+r+'_Choh\t'+r+'_Caa')
            if r == 'GLU': output.write('\n')
            else: output.write('\t')
    #going trough temp ranges
    count = {}
    for range_nr in range(len(imput)):
        print "-"
        print "reading data from selectfile "+str(range_nr)
        print "-"
        count[range_nr] = 0
        imput2=imput[range_nr]
        pdblist = open(imput2,'r')
        for line in pdblist.readlines():
            if line[0] == '#': pass
            else:
                pdbId = line[8:12]
                chain_id = line[12]
                if pdbId in filter: 
                    print "passing "+pdbId
                    pass
                else:
                    count[range_nr] += 1
                    try:
                        output.write(pdbId+'_'+chain_id+'\t')
                        output.write(temp_extract(pdbId)+'\t')#withought this the skrypt is 10x faster!
                        (buried,not_buried,Choh,Caa) = readChain(pdbId,chain_id)
                        for i in range(21):
                            r = res[i]
                            if r == 'HOH': pass
                            else:
                                output.write(str(buried[r])+'\t'+str(not_buried[r])+'\t'+str(Choh[r])+'\t'+str(Caa[r]))
                                if r == 'GLU': output.write('\n')
                                else: output.write('\t')
                    except TypeError:
                        output.write('\n')
                        pass
    print count
    if len(filter_append_list)>0:
        output.close()
        bla = open(filter_file,'w')
        bla.write('\t'.join(filter)+'\t'+'\t'.join(filter_append_list))
        bla.close()
        print "filter updated, you should run it again."
    print "done"