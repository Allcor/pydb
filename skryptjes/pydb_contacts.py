#! /usr/bin/env python
'''
20-1-2014
Arlo Hoogeveen
calculating pair potentials
'''

import math,gzip

k = 1#1.3806488 #10^23
T = 1#298 # 25 degrees celcius
#res = {0:'ALA',1:'ARG',2:'ASN',3:'ASP',4:'CYS',5:'GLU',6:'GLN',7:'GLY',8:'HIS',9:'ILE',10:'LEU',11:'LYS',12:'MET',13:'PHE',14:'PRO',15:'SER',16:'THR',17:'TRP',18:'TYR',19:'VAL'}
#resNr = {'ALA':0,'ARG':1,'ASN':2,'ASP':3,'CYS':4,'GLU':5,'GLN':6,'GLY':7,'HIS':8,'ILE':9,'LEU':10,'LYS':11,'MET':12,'PHE':13,'PRO':14,'SER':15,'THR':16,'TRP':17,'TYR':18,'VAL':19}
res = {0:'CYS',1:'PHE',2:'LEU',3:'TRP',4:'VAL',5:'ILE',6:'MET',7:'HIS',8:'TYR',9:'ALA',10:'GLY',11:'PRO',12:'ASN',13:'THR',14:'SER',15:'ARG',16:'GLN',17:'ASP',18:'LYS',19:'GLU',20:'HOH'}
resNr = {'CYS':0,'PHE':1,'LEU':2,'TRP':3,'VAL':4,'ILE':5,'MET':6,'HIS':7,'TYR':8,'ALA':9,'GLY':10,'PRO':11,'ASN':12,'THR':13,'SER':14,'ARG':15,'GLN':16,'ASP':17,'LYS':18,'GLU':19,'HOH':20}
#resSurfArea = {'CYS':127,'PHE':201,'LEU':203,'TRP':273,'VAL':201,'ILE':217,'MET':252,'HIS':257,'TYR':288,'ALA':174,'GLY':143,'PRO':174,'ASN':200,'THR':158,'SER':202,'ARG':276,'GLN':246,'ASP':229,'LYS':256,'GLU':254}
resSurfArea = {'ARG':241,'TRP':259,'TYR':229,'LYS':211,'PHE':218,'MET':204,'GLN':189,'HIS':194,'GLU':183,'LEU':180,'ILE':182,'ASN':158,'ASP':151,'CYS':140,'VAL':160,'THR':146,'PRO':143,'SER':122,'ALA':113,'GLY':85}
resLetter = {'C':'CYS','F':'PHE','L':'LEU','W':'TRP','V':'VAL','I':'ILE','M':'MET','H':'HIS','Y':'TYR','A':'ALA','G':'GLY','P':'PRO','N':'ASN','T':'THR','S':'SER','R':'ARG','Q':'GLN','D':'ASP','K':'LYS','E':'GLU'}

output = "ppmatrix3.txt"
imput = "../selectfile3.txt"
pdbfolder = '../pdb_files/pdb'
dsspFolder = "../DSSP_files/"
filter_file = "./errorlog2.txt"
filter = []
for line in open(filter_file,'r').readlines():
    for id in line.strip().split('\t'):
        filter.append(id)

def pairpot(x,y):
    ksum = 0
    for i in range(20):
        ksum += n[res[i]]
    w = float((n[res[x]]*n[res[y]]))/float(ksum)
    pp = -1*k*T*math.log(c[x][y]/w)
    return pp

def readAllChains():
    sequenceDist=2
    sqCutoff = 12.0**2.0
    n = {}
    #q = {} # could remove q, was just to check out if 4 comes close
    for r in resNr.keys():
        n[r] = 0
        #q[r] = 0
    ksum = 0
    c = [[0]*21 for i in range(21)]
    pdblist = open(imput,'r')
    for line in pdblist.readlines():
        if line[0] == '#':pass
        else:
            try:
                pdbId = line[8:12]
                chain_id = line[12]
                print 'reading: '+pdbId +' '+chain_id
                if pdbId in filter:
                    pass
                else:
                    residues = {} #clearing privious chain't residue info
                    filename = pdbfolder+pdbId.lower()+'.ent.gz'
                    try:
                        pdbfile = gzip.open(filename, 'r') 
                    except IOError: # In case of IOError return empty collection
                        print "Error: Cannot open PDB file " + filename + "."
                        # exit(1)
                        return
                    for line in pdbfile.readlines():
                        line =  line.rstrip()
                        first4 = line[0:4]
                        if(first4 == "ATOM"):
                            if chain_id == line[21]:
                                if line[12:16].strip() == 'CB' or line[17:20].strip()=='GLY' and line[12:16].strip() == 'C':
                                    res_num  = int(line[22:26])
                                    if residues.get(res_num) == None:
                                        aa_type = line[17:20].strip().upper()
                                        xcoord = float(line[30:38])
                                        ycoord = float(line[38:46])
                                        zcoord = float(line[46:54])
                                        atom = [aa_type,xcoord,ycoord,zcoord,[],0]
                                        residues[res_num] = atom
                    pdbfile.close()
                    #calculating contacts
                    for j in residues.keys():
                        xj = residues[j][1]
                        yj = residues[j][2]
                        zj = residues[j][3]
                        for i in residues.keys():
                            if(i<(j-sequenceDist)):
                                xi = residues[i][1]
                                yi = residues[i][2]
                                zi = residues[i][3]
                                sqDist = (xi-xj)**2 + (yi-yj)**2 + (zi-zj)**2
                                if(sqDist < sqCutoff):
                                    #neighbour list
                                    residues[j][4].append([sqDist,i])
                                    residues[i][4].append([sqDist,j])
                    #water contacts
                    dsspdoc = open(dsspFolder+pdbId+'.dssp','r')
                    for line in dsspdoc.readlines():
                        if line.strip()[-1] == '.':pass
                        elif line.strip()[0] == '#':pass
                        else:
                            if line[11] == chain_id:
                                res_num = int(line[5:10].strip())
                                if line[13] == 'X':
                                    print "skipping res "+str(res_num)
                                else:
                                    #aa_type can be a lowercase letter in the dssp, indicating CYS sulphur bindings, a with a, b with b exc. 
                                    if line[13].islower():
                                        #print "lower :"+ line[13]+' in '+pdbId
                                        aa_type = 'CYS'
                                    else:
                                        aa_type = resLetter[line[13]]
                                    if residues.get(res_num) == None:
                                        print "can't find res_"+str(res_num)+" in pdb dictionary"
                                    acc = float(line[34:38].strip())
                                    #if acc > resSurfArea[aa_type]:
                                    #    print aa_type+" max surface should be "+str(acc)
                                    ar = acc/resSurfArea[aa_type]
                                    residues[res_num][5] = ar
                    dsspdoc.close()
                    #updating amount of residues and contacts
                    for j in residues.keys():
                        #water contacts
                        solv_contact = int(residues[j][5]*4+0.5)
                        n[residues[j][0]] += solv_contact
                        n['HOH'] += solv_contact
                        c[resNr[residues[j][0]]][20] += solv_contact
                        c[20][resNr[residues[j][0]]] += solv_contact
                            #res contacts
                        num_neighbours = int(4*(1-residues[j][5])+0.5)
                        x = resNr[residues[j][0]]
                        sorted_distences = residues[j][4]
                        sorted_distences.sort()
                        for a in sorted_distences[:num_neighbours]:
                            i = a[1]
                            if i>j:
                                y = resNr[residues[i][0]]
                                n[residues[j][0]] += 1
                                n[residues[i][0]] += 1
                                c[x][y] += 1
                                c[y][x] += 1
                        if solv_contact+len(sorted_distences) <= 4:
                            print "less then 4 contacts detected! on res "+str(j)
                
            except KeyError:
                print "skipping "+pdbId+", KeyError (probebly pdb and dssp don't line up)"
    pdblist.close()
    return[c,n]

if __name__ == "__main__":
    #making n,c and som
    foo = readAllChains()
    c = foo[0]
    n = foo[1]
    matrix = [[]*21 for i in range(21)]
    output = open(output,'w')
    output.write('\t'+'\t'.join(res.values())+'\n')
    for i in range(21):
        for j in range(21):
            #if j<=i:
            if i == 20 and j == 20:
                matrix[i].append("0")
            else:
                matrix[i].append(str("%.2f" % pairpot(i,j)))
        output.write(res[i]+'\t'+'\t'.join(matrix[i])+'\n')
    output.close()
    print "done"